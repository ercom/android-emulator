# Android emulator service

Example of use:
```bash
$ docker run --rm --privileged -e ANDROID_EMULATOR_API=android-25 -e ANDROID_EMULATOR_ARCH=x86 --network host registry.gitlab.com/ercom/android-emulator:25
```
